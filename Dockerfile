FROM debian:stable
USER root

RUN apt-get update -y && apt-get install -y --force-yes apache2
ADD Invidiot/dist/Invidiot/ /var/www/html/

EXPOSE 80
VOLUME ["/var/www", "/var/log/apache2", "/etc/apache2"]
ENTRYPOINT ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]