# Invidiot Server

Hosts a Docker-ized web app that redirects from youtu.be links, to invidio.us.

### Installation

```./build.sh && ./install.sh && ./run.sh```

### Prerequisites:

1. Docker
2. Unused TCP port 80.