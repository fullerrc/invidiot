import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  urlForm: FormGroup;

  constructor(@Inject(DOCUMENT) private document: any) {}

  ngOnInit() {
    this.createForm();
  }

  createForm () {
    this.urlForm = new FormGroup({
      url: new FormControl(null, [Validators.required, this.isYoutubeUrl]),
    });
  }

  getUrl () {
    const { url } = this.urlForm.value;
    const urlSplit = url.split('/');
    if (urlSplit && urlSplit.length > 1) {
      urlSplit[2] = 'invidio.us';
    }
    this.document.location.href = urlSplit.join('/');;
  }

  get isValid(): boolean {
    return this.urlForm && this.urlForm.valid;
  }

  isYoutubeUrl (control: FormControl) {
    if (control.value) {
      const urlSplit = control.value.split('/');
      if (!urlSplit || urlSplit.length < 3 || urlSplit[2] !== 'youtu.be') {
        return {
          invalidUrl: true,
        };
      }
    }
    return null;
  }
}
